# Transkribus HTR Pipeline

![release badge](https://gitlab.ub.uni-bielefeld.de/sfb1288inf/transkribus-htr-pipeline/-/badges/release.svg)
![pipeline badge](https://gitlab.ub.uni-bielefeld.de/sfb1288inf/transkribus-htr-pipeline/badges/main/pipeline.svg?ignore_skipped=true)

This software implements a heavily parallelized pipeline to recognize handwritten text in PDF files. It is used for nopaque's HTR service but you can also use it standalone, for that purpose a convenient wrapper script is provided.

## Software used in this pipeline implementation

- Official Debian Docker image (buster-slim): https://hub.docker.com/_/debian
  - Software from Debian Buster's free repositories
- ocropy (1.3.3): https://github.com/ocropus/ocropy/releases/tag/v1.3.3
- pyFlow (1.1.20): https://github.com/Illumina/pyflow/releases/tag/v1.1.20
- Transkribus Processing API (1.0-beta): https://transkribus.eu/processing/swagger/

## Installation

1. Install Docker and Python 3.
2. Clone this repository: `git clone https://gitlab.ub.uni-bielefeld.de/sfb1288inf/transkribus-htr-pipeline.git`
3. Build the Docker image: `docker build -t transkribus-htr-pipeline:latest transkribus-htr-pipeline`
4. Add the wrapper script (`wrapper/transkribus-htr-pipeline` relative to this README file) to your `${PATH}`.
5. Create working directories for the pipeline: `mkdir -p /<my_data_location>/{input,output}`.

## Use the Pipeline

1. Place your PDF files inside `/<my_data_location>/input`. Files should all contain text of the same language.
2. Clear your `/<my_data_location>/output` directory.
3. Start the pipeline process. Check the pipeline help (`transkribus-htr-pipeline --help`) for more details.
```bash
cd /<my_data_location>
transkribus-htr-pipeline \
  --input-dir input \
  --output-dir output \
  -m <model_code> <optional_pipeline_arguments>
```
4. Check your results in the `/<my_data_location>/output` directory.
